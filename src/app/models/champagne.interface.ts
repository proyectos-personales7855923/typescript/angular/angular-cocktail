import { Drink } from "./drink.interface";

export interface Champagne {
    drinks: Drink[];
}

