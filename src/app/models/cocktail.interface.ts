import { Drink } from "./drink.interface";

export interface Cocktail extends Drink {
    drinks: Drink[];
}

